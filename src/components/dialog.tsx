import { h } from 'preact';
import { Dialog } from './app';

type Props = {
    dialog: Dialog
}

export const DialogComponent = (props: Props) => {
    const onActorNameChanged = (e: any) => {
        if (e.target?.value)
        {
            props.dialog.actor = e.target.value;
        }
    }

    const onTextChanged = (e: any) => {
        if (e.target?.value)
        {
            props.dialog.text = e.target.value;
        }
    }

    const onEmotionChanged = (e: any) => {
        if (e.target?.value)
        {
            props.dialog.emotion = e.target.value;
        }
    }

    const onLeftChanged = (e: any) => {
        props.dialog.left = e.target.checked;
    }

    const onPortraitChanged = (e: any) => {
        props.dialog.portrait = e.target.files[0].name.slice(0, e.target.files[0].name.indexOf('.')).slice(e.target.files[0].name.indexOf('_') + 1);

        let index = props.dialog.portrait.indexOf('_');
        while(index !== -1)
        {
            let prevCharacter = props.dialog.portrait[index + 1];
            let newCharacter = prevCharacter.toUpperCase();

            props.dialog.portrait = props.dialog.portrait.substr(0, index) + newCharacter + props.dialog.portrait.substr(index + 2);
            index = props.dialog.portrait.indexOf('_');
        }

        console.log(props.dialog.portrait);
    }

    const onConditionChanged = (e: any) =>
    {
        if (e.target?.value)
        {
            props.dialog.condition = e.target.value;
        }
    }

    const onConditionDirectionChanged = (e: any) => {
        props.dialog.conditionDirection = e.target.checked;
    }

    return (
        <div className='dialog'>
            <div>
                <label for='actorName'>
                    actor
                </label>
                <input id='actorName' type='text' value={props.dialog.actor} onInput={onActorNameChanged} />
            </div>
            <div>
                <label for='text'>
                    text
                </label>
                <input className='text' id='text' type='text' value={props.dialog.text} onInput={onTextChanged} />
            </div>
            <div>
                <label for='emotion'>
                    emotion
                </label>
                <select className='emotion' id='emotion' type='emotion' value={props.dialog.emotion} onInput={onEmotionChanged}>
                    <option value='neutral'>neutral</option>
                    <option value='angry'>angry</option>
                    <option value='annoyed'>annoyed</option>
                    <option value='eager'>eager</option>
                    <option value='sad'>sad</option>
                    <option value='thought'>thought</option>
                </select>
            </div>
            <div>
                <label for='left'>
                    left
                </label>
                <input id='left' type='checkbox' checked={props.dialog.left} onInput={onLeftChanged} />
            </div>
            <div>
                <label for='portrait'>
                    portrait
                </label>
                <input id='portrait' type='file' onChange={onPortraitChanged} />
            </div>
            <div>
                <label for='condition'>
                    condition
                </label>
                <input className='condition' id='condition' type='condition' value={props.dialog.condition} onChange={onConditionChanged} />
            </div>
            <div>
                <label for='conditionDirection'>
                    conditionDirection
                </label>
                <input id='conditionDirection' type='checkbox' checked={props.dialog.conditionDirection} onInput={onConditionDirectionChanged} />
            </div>
        </div>
    );
};
