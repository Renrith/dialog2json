import { FunctionalComponent, h } from 'preact';
import { useState } from 'preact/hooks';

import { DialogComponent } from './dialog';

export type Dialog = {
    actor: string
    text: string
    emotion: string
    left: boolean
    portrait: string
    condition: string
    conditionDirection: boolean
}

const App: FunctionalComponent = () => {
    const [dialogArray, useDialogArray] = useState([] as Array<Dialog>);
    const [JSONResult, useJSONResult] = useState('');

    const onTextChanged = (e: any) =>
    {
        let dialogues = e.target?.value as string;

        dialogues = dialogues.replace(/[“”]/g, '"');
        dialogues = dialogues.replace(/[’]/g, "'");
        dialogues = dialogues.replace(/[…]/g, "...");

        const newDialogArray = [];

        while(dialogues.indexOf(':') !== -1)
        {
            let actor = dialogues.slice(0, dialogues.indexOf(':'));
            let index = actor.lastIndexOf(' ');
            if(index !== -1)
            {
                actor = actor.slice(index);
                actor = actor.slice(actor.indexOf(' ') + 1);
            }
            actor = actor.toLowerCase();
            let actorFirstLetter = actor.charAt(0).toUpperCase();
            actor = actorFirstLetter + actor.slice(1);

            dialogues = dialogues.slice(dialogues.indexOf(':') + 1);

            let text = dialogues.slice(dialogues.indexOf('"') + 1);
            text = text.slice(0, text.indexOf('"'));

            dialogues = dialogues.slice(dialogues.indexOf('"') + 1);
            dialogues = dialogues.slice(dialogues.indexOf('"') + 1);

            newDialogArray.push({actor, text, left: true, emotion: "", portrait: "", condition: "", conditionDirection: true});
        }
        useDialogArray(newDialogArray);
    }

    const onGenerateJSON = () =>
    {
        let result = JSON.stringify(dialogArray);
        useJSONResult(result);
    }

    return (
        <div className='root'>
            <input className='inputText' id='text' type='textArea' onInput={onTextChanged} />
            {
                dialogArray.map(dialog => <DialogComponent dialog={dialog} />)
            }
            <button className='button' onClick={onGenerateJSON}>Generate JSON</button>
            {JSONResult != '' &&
                <input className='outputText' id='text' type='textArea' value={JSONResult} />
            }
        </div>
    );
};

export default App;
